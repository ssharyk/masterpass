package free.ssharyk.masterpass.usecase.auth

import free.ssharyk.masterpass.cipher.CipherService
import free.ssharyk.masterpass.database.entity.UserRecord
import free.ssharyk.masterpass.database.repo.IUserDatabaseRepository
import free.ssharyk.masterpass.store.runtime.RuntimeService
import javax.inject.Inject

class AuthUseCase @Inject constructor(
    private val cipher: CipherService,
    private val runtime: RuntimeService,
    private val db: IUserDatabaseRepository,
) : AuthService {

    override suspend fun hasRecords(): Boolean {
        return db.usersCount() > 0
    }

    override suspend fun login(name: String, password: String): LoginResult {
        val hashedPassword = cipher.calculateHash(password) ?: return LoginCipherError
        val user = db.findUserByLogin(name)
        return if (user == null) {
            // нет пользователя с таким логином
            LoginUserNotFound
        } else {
            if (user.passwordHash == hashedPassword) {
                // совпадение - сохраняем пароль в памяти
                saveRuntime(user, password)
                LoginSuccess
            } else {
                // имя верное, но пароль не соответствует
                LoginInvalidPassword
            }
        }
    }

    override suspend fun register(name: String, password: String): SignUpResult {
        val hashedPassword = cipher.calculateHash(password) ?: return SignUpCipherError
        val user = db.findUserByLogin(name)
        return if (user != null) {
            // уже есть такой пользователь
            SignUpLoginUsedAlready
        } else {
            // создаем нового
            val newUser = db.insertUser(name, hashedPassword)
            if (newUser == null) {
                SignUpStoreError
            } else {
                saveRuntime(newUser, password)
                SignUpSuccess
            }
        }
    }

    /**
     * Сохранение данных активного пользователя в памяти
     */
    private fun saveRuntime(user: UserRecord, plainPassword: String) {
        runtime.masterPass = plainPassword
        runtime.currentUserId = user.id
    }
}