package free.ssharyk.masterpass.usecase.passwordcontent

import free.ssharyk.masterpass.usecase.entity.Password
import free.ssharyk.masterpass.usecase.entity.PasswordGenerationOption

/**
 * Основной API работы с паролями - чтение и запись
 */
interface PasswordsService {

    /**
     * Получение всех паролей активного пользователя
     */
    suspend fun getAllPasswords(): List<Password>

    /**
     * Получение полных данных о пароле с указанным [id], принадлежащего активному пользователю
     */
    suspend fun getPassword(id: Long): Password?

    /**
     * Обновить пароль в хранилище
     */
    suspend fun update(password: Password): Boolean

    /**
     * Создать новый пароль с указанными требованиями
     */
    fun createRandomPassword(length: Int, options: Set<PasswordGenerationOption>): String

}