package free.ssharyk.masterpass.usecase.entity

/**
 * Опции генерации случайного пароля
 */
enum class PasswordGenerationOption {

    /**
     * Включить спец. символы: . , ! ? # * _
     */
    SPECIAL_CHARS,

    /**
     * Включить цифры
     */
    DIGITS,

    /**
     * Символы в верхнем и нижнем регистре
     */
    TWO_CASES

}