package free.ssharyk.masterpass.usecase.auth

sealed class LoginResult

object LoginSuccess: LoginResult()

/**
 * Внутренняя ошибка шифрования
 */
object LoginCipherError: LoginResult()

/**
 * Есть пользователь с таким логином, но пароль не совпадает
 */
object LoginInvalidPassword: LoginResult()

/**
 * Нет пользователя с таким логином
 */
object LoginUserNotFound: LoginResult()