package free.ssharyk.masterpass.usecase.auth

interface AuthService {

    /**
     * Проверяет возможность входа (согласование логина [name] и пароля [password]
     * с теми, что сохранены в хранилище)
     */
    suspend fun login(name: String, password: String): LoginResult

    /**
     * Добавляет новую запись в хранилище
     */
    suspend fun register(name: String, password: String): SignUpResult

    /**
     * Проверет наличие в системе хотя бы одной учетной записи
     */
    suspend fun hasRecords(): Boolean

}