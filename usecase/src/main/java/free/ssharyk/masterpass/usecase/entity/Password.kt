package free.ssharyk.masterpass.usecase.entity

/**
 * Сохраненный пароль, готовый к отображению - должен храниться только в памяти
 */
data class Password(
    val id: Long,
    val name: String,
    val plainPassword: String,
    val link: String,
    val description: String?,
    val lastUpdateTimestamp: Long,
)