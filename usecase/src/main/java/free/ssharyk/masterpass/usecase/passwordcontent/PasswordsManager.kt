package free.ssharyk.masterpass.usecase.passwordcontent

import free.ssharyk.masterpass.cipher.CipherService
import free.ssharyk.masterpass.database.repo.IPasswordDatabaseRepository
import free.ssharyk.masterpass.store.runtime.RuntimeService
import free.ssharyk.masterpass.usecase.entity.Password
import free.ssharyk.masterpass.usecase.entity.PasswordDb
import free.ssharyk.masterpass.usecase.entity.PasswordGenerationOption
import javax.inject.Inject

/**
 * Реализация менеджера паролей
 */
class PasswordsManager @Inject constructor(
    private val runtime: RuntimeService,
    private val db: IPasswordDatabaseRepository,
    private val cipher: CipherService
) : PasswordsService {

    private val fromDbPasswordTransformer: (PasswordDb) -> Password? = {
        val plainPass = cipher.decrypt(it.encryptedPassword, runtime.masterPass)
        if (plainPass == null) {
            null
        } else {
            Password(
                id = it.id,
                name = it.name,
                plainPassword = plainPass,
                link = it.link,
                description = it.description,
                lastUpdateTimestamp = it.timestamp
            )
        }
    }

    override suspend fun getAllPasswords(): List<Password> {
        val userId = runtime.currentUserId
        return if (userId == RuntimeService.NO_USER_ID || runtime.masterPass == RuntimeService.NO_PASSWORD) {
            emptyList()
        } else {
            db.findPasswordsByUserId(userId).mapNotNull(fromDbPasswordTransformer)
        }
    }

    override suspend fun getPassword(id: Long): Password? {
        val userId = runtime.currentUserId
        return if (userId == RuntimeService.NO_USER_ID || runtime.masterPass == RuntimeService.NO_PASSWORD) {
            null
        } else {
            val pass = db.findPasswordById(id) ?: return null
            if (pass.userId == userId) {
                fromDbPasswordTransformer(pass)
            } else {
                null
            }
        }
    }

    override suspend fun update(password: Password): Boolean {
        val userId = runtime.currentUserId
        val masterPass = runtime.masterPass
        return if (userId == RuntimeService.NO_USER_ID || masterPass == RuntimeService.NO_PASSWORD) {
            false
        } else {
            val encPass = cipher.encrypt(password.plainPassword, masterPass)
            if (encPass == null) {
                false
            } else {
                val updated = db.insertOrReplacePassword(
                    id = password.id,
                    userId = userId,
                    title = password.name,
                    link = password.link,
                    encryptedPassword = encPass,
                    description = password.description
                )
                updated != null
            }
        }
    }

    override fun createRandomPassword(
        length: Int,
        options: Set<PasswordGenerationOption>
    ): String {
        return cipher.generate(
            length = length,
            includeDigits = PasswordGenerationOption.DIGITS in options,
            includeSpecialChars = PasswordGenerationOption.SPECIAL_CHARS in options,
            includeTwoCases = PasswordGenerationOption.TWO_CASES in options
        )
    }

}