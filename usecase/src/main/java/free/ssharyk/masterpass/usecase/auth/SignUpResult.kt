package free.ssharyk.masterpass.usecase.auth

sealed class SignUpResult

object SignUpSuccess: SignUpResult()

/**
 * Внутренняя ошибка шифрования
 */
object SignUpCipherError: SignUpResult()

/**
 * Внутренняя ошибка, связанная с хранилищем (чтение/запись)
 */
object SignUpStoreError: SignUpResult()

/**
 * Пользователь с таким логином уже есть в системе
 */
object SignUpLoginUsedAlready: SignUpResult()