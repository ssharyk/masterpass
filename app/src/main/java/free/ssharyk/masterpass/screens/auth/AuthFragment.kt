package free.ssharyk.masterpass.screens.auth

import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.annotation.StringRes
import androidx.fragment.app.viewModels
import androidx.navigation.NavDirections
import androidx.navigation.fragment.findNavController
import dagger.hilt.android.AndroidEntryPoint
import free.ssharyk.masterpass.BuildConfig
import free.ssharyk.masterpass.R
import free.ssharyk.masterpass.databinding.FragmentAuthBinding
import free.ssharyk.masterpass.screens.BaseFragment
import free.ssharyk.masterpass.usecase.auth.*
import free.ssharyk.masterpass.validation.LoginValidator
import free.ssharyk.masterpass.validation.PasswordValidator

@AndroidEntryPoint
class AuthFragment :
    BaseFragment<FragmentAuthBinding, AuthViewModel>(FragmentAuthBinding::inflate) {

    override val viewModel: AuthViewModel by viewModels()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel.hasUsers.observe(viewLifecycleOwner) { hasUsers ->
            binding.authButtonLogin.visibility = if (hasUsers) View.VISIBLE else View.INVISIBLE
        }

        subscribeActionLoginButton()
        subscribeActionRegisterButton()

        if (BuildConfig.DEBUG) {
            binding.authLoginInput.setText("aaaaaaaa")
            binding.authPasswordInput.setText("aaaaaaaaaaaa")
        }
    }

    /**
     * Назначить реакцию на нажатия кнопки "войти"
     */
    private fun subscribeActionLoginButton() {
        binding.authButtonLogin.setOnClickListener {
            clearErrors()

            val isValid = validateAndShowErrors()
            if (!isValid) {
                return@setOnClickListener
            }

            viewModel.login(
                binding.authLoginInput.text.toString(),
                binding.authPasswordInput.text.toString()
            ) {
                when (it) {
                    is LoginSuccess -> {
                        launchPasswordsListScreen()
                    }

                    is LoginInvalidPassword,
                    is LoginUserNotFound,
                    is LoginCipherError -> {
                        showError(R.string.error_incorrect_data)
                    }
                }
            }
        }
    }

    /**
     * Назначить реакцию на нажатия кнопки "создать аккаунт"
     */
    private fun subscribeActionRegisterButton() {
        binding.authButtonRegister.setOnClickListener {
            clearErrors()

            val isValid = validateAndShowErrors()
            if (!isValid) {
                return@setOnClickListener
            }

            viewModel.register(
                binding.authLoginInput.text.toString(),
                binding.authPasswordInput.text.toString()
            ) {
                when (it) {
                    is SignUpSuccess -> {
                        launchPasswordsListScreen()
                    }

                    is SignUpLoginUsedAlready -> {
                        showError(R.string.error_signup_used_already)
                    }

                    is SignUpStoreError,
                    is SignUpCipherError -> {
                        showError(R.string.error_incorrect_data)
                    }
                }
            }
        }
    }

    /**
     * Проверка формата введенных данных
     */
    private fun validateAndShowErrors(): Boolean {
        var allParamsOk = true
        val login = binding.authLoginInput.text.toString()
        if (!LoginValidator.isLoginValid(login)) {
            binding.authLoginInput.error = loadString(R.string.error_invalid_login_format)
            allParamsOk = false
        }

        val password = binding.authPasswordInput.text.toString()
        if (!PasswordValidator.isMasterPasswordValid(password)) {
            binding.authPasswordInput.error = loadString(R.string.error_invalid_password_format)
            allParamsOk = false
        }

        return allParamsOk
    }

    private fun clearErrors() {
        binding.authLoginInput.error = null
        binding.authPasswordInput.error = null
    }

    private fun loadString(@StringRes id: Int): String = resources.getString(id)

    private fun showError(@StringRes err: Int) {
        Toast.makeText(context, err, Toast.LENGTH_SHORT).show()
    }

    private fun launchPasswordsListScreen() {
        val direction = AuthFragmentDirections.actionAuthFragmentToPasswordsListFragment()
        findNavController().navigate(direction)
    }

}