package free.ssharyk.masterpass.screens.list

import android.os.Bundle
import android.view.View
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import dagger.hilt.android.AndroidEntryPoint
import free.ssharyk.masterpass.databinding.FragmentPasswordsListBinding
import free.ssharyk.masterpass.screens.BaseFragment
import free.ssharyk.masterpass.usecase.entity.Password

@AndroidEntryPoint
class PasswordsListFragment :
    BaseFragment<FragmentPasswordsListBinding, PasswordsListViewModel>(FragmentPasswordsListBinding::inflate),
    PasswordsListAdapter.PasswordItemClickListener {

    override val viewModel: PasswordsListViewModel by viewModels()
    private lateinit var adapter: PasswordsListAdapter

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        adapter = PasswordsListAdapter()
        adapter.passwordItemClickListener = this
        binding.passwordListRv.layoutManager = LinearLayoutManager(context)
        binding.passwordListRv.adapter = adapter
        viewModel.retrieveUserPasswords {
            adapter.submitList(it)
        }

        subscribeAddNewPasswordButton()
    }

    override fun onPasswordItemClick(position: Int, password: Password) {
        launchPasswordDetailsScreen(password.id)
    }

    private fun subscribeAddNewPasswordButton() {
        binding.passwordListButtonAddNew.setOnClickListener {
            launchPasswordDetailsScreen(0L)
        }
    }

    private fun launchPasswordDetailsScreen(passwordId: Long) {
        val destination = PasswordsListFragmentDirections.actionPasswordsListFragmentToPasswordDetailsFragment(passwordId)
        findNavController().navigate(destination)
    }

}