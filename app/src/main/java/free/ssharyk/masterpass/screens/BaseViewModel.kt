package free.ssharyk.masterpass.screens

import androidx.lifecycle.ViewModel

open class BaseViewModel : ViewModel()