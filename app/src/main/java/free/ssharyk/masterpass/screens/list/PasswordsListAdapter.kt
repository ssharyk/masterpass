package free.ssharyk.masterpass.screens.list

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import free.ssharyk.masterpass.databinding.ListItemPasswordBinding
import free.ssharyk.masterpass.extensions.formattedDate
import free.ssharyk.masterpass.usecase.entity.Password

class PasswordsListAdapter :
    ListAdapter<Password, PasswordsListAdapter.PasswordViewHolder>(DIFF_UTIL_CALLBACK) {

    fun interface PasswordItemClickListener {
        fun onPasswordItemClick(position: Int, password: Password)
    }

    companion object {
        private val DIFF_UTIL_CALLBACK = object : DiffUtil.ItemCallback<Password>() {
            override fun areItemsTheSame(oldItem: Password, newItem: Password): Boolean {
                return oldItem.id == newItem.id
            }

            override fun areContentsTheSame(oldItem: Password, newItem: Password): Boolean {
                return oldItem == newItem
            }
        }
    }

    var passwordItemClickListener: PasswordItemClickListener? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PasswordViewHolder {
        val itemBinding =
            ListItemPasswordBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return PasswordViewHolder(itemBinding, passwordItemClickListener) {
            getItem(it)
        }
    }

    override fun onBindViewHolder(holder: PasswordViewHolder, position: Int) {
        holder.bind(getItem(position))
    }

    class PasswordViewHolder(
        private val itemBinding: ListItemPasswordBinding,
        clickListener: PasswordItemClickListener?,
        itemProvider: (Int) -> Password
    ) : RecyclerView.ViewHolder(itemBinding.root) {

        init {
            itemBinding.root.setOnClickListener {
                val pos = adapterPosition
                clickListener?.onPasswordItemClick(
                    pos,
                    itemProvider(pos)
                )
            }
        }

        fun bind(password: Password) {
            itemBinding.passwordListItemHeader.text = password.name
            itemBinding.passwordListItemLink.text = password.link
            itemBinding.passwordListItemDescription.text = password.description
            itemBinding.passwordListItemLastUpdate.text =
                password.lastUpdateTimestamp.formattedDate()
        }

    }

}