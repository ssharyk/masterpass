package free.ssharyk.masterpass.screens.details

import android.os.Bundle
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import android.widget.SeekBar
import android.widget.Toast
import androidx.annotation.StringRes
import androidx.core.widget.doOnTextChanged
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import androidx.navigation.ui.NavigationUI
import dagger.hilt.android.AndroidEntryPoint
import free.ssharyk.masterpass.R
import free.ssharyk.masterpass.databinding.FragmentPasswordDetailsBinding
import free.ssharyk.masterpass.databinding.LayoutGenerationOptionsBinding
import free.ssharyk.masterpass.extensions.formattedDate
import free.ssharyk.masterpass.screens.BaseFragment
import free.ssharyk.masterpass.usecase.entity.Password
import free.ssharyk.masterpass.usecase.entity.PasswordGenerationOption
import free.ssharyk.masterpass.validation.PasswordValidator

@AndroidEntryPoint
class PasswordDetailsFragment :
    BaseFragment<FragmentPasswordDetailsBinding, PasswordDetailsViewModel>(
        FragmentPasswordDetailsBinding::inflate
    ) {

    override val viewModel: PasswordDetailsViewModel by viewModels()
    private val passwordDetailsNavArgs: PasswordDetailsFragmentArgs by navArgs()
    private lateinit var optionsBinding: LayoutGenerationOptionsBinding

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setHasOptionsMenu(true)

        optionsBinding = LayoutGenerationOptionsBinding.bind(binding.root)
        setupHints()

        val passId = passwordDetailsNavArgs.passwordId
        viewModel.load(passId)

        viewModel.password.observe(viewLifecycleOwner) {
            renderPassword(it)
        }

        subscribeTextWatchers()
        subscribeGeneratePasswordClick()
    }

    /**
     * Вывести данные самого пароля
     */
    private fun renderPassword(password: Password) {
        binding.passwordDetailsDomainNameInput.setText(password.name)
        binding.passwordDetailsDomainLinkInput.setText(password.link)
        binding.passwordDetailsDomainDescriptionInput.setText(password.description)
        binding.passwordDetailsPasswordInput.setText(password.plainPassword)
        binding.passwordDetailsLastUpdate.text = password.lastUpdateTimestamp.formattedDate()
    }

    /**
     * Реакция на изменение текста
     */
    private fun subscribeTextWatchers() {
        binding.passwordDetailsDomainNameInput.doOnTextChanged { text, _, _, _ ->
            viewModel.updateName(text)
        }
        binding.passwordDetailsDomainLinkInput.doOnTextChanged { text, _, _, _ ->
            viewModel.updateLink(text)
        }
        binding.passwordDetailsDomainDescriptionInput.doOnTextChanged { text, _, _, _ ->
            viewModel.updateDescription(text)
        }
        binding.passwordDetailsPasswordInput.doOnTextChanged { text, _, _, _ ->
            viewModel.updatePassword(text)
        }

        optionsBinding.generationOptionsLengthSlider.setOnSeekBarChangeListener(object :
            SeekBar.OnSeekBarChangeListener {
            override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {
                setupLengthIndicator(progress + PasswordValidator.MIN_LENGTH)
            }

            override fun onStartTrackingTouch(seekBar: SeekBar?) {
            }

            override fun onStopTrackingTouch(seekBar: SeekBar?) {
            }
        })
    }

    /**
     * Реакция на нажатия кнопки "сгенерировать случайный пароль"
     */
    private fun subscribeGeneratePasswordClick() {
        binding.passwordDetailsButtonGenerateRandom.setOnClickListener {
            val length = optionsBinding.generationOptionsLengthSlider.progress
            val options = mutableSetOf<PasswordGenerationOption>().apply {
                if (optionsBinding.generationOptionsIncludeDigitsToggle.isChecked) add(
                    PasswordGenerationOption.DIGITS
                )
                if (optionsBinding.generationOptionsIncludeSpecialCharsToggle.isChecked) add(
                    PasswordGenerationOption.SPECIAL_CHARS
                )
                if (optionsBinding.generationOptionsIncludeTwoCasesToggle.isChecked) add(
                    PasswordGenerationOption.TWO_CASES
                )
            }
            viewModel.generateRandomPassword(length, options)
        }
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.menu_details, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == R.id.miSave) {
            savePassword()
            return true
        }
        return NavigationUI.onNavDestinationSelected(item, findNavController())
                || super.onOptionsItemSelected(item)
    }

    /**
     * Проверить данные и в случае корректности выполнить занесение их в хранилище
     */
    private fun savePassword() {
        val name = binding.passwordDetailsDomainNameInput.text.toString()
        val link = binding.passwordDetailsDomainLinkInput.text.toString()
        val description = binding.passwordDetailsDomainDescriptionInput.text.toString()
        val plainPass = binding.passwordDetailsPasswordInput.text.toString()

        val isValid = validateAndShowErrors()
        if (!isValid) {
            return
        }

        viewModel.save(name, link, description, plainPass) { savedSuccess ->
            if (savedSuccess) {
                Toast.makeText(context, R.string.saved_success, Toast.LENGTH_SHORT).show()
                findNavController().popBackStack()
            } else {
                showError(R.string.error_save_failure)
            }
        }
    }

    /**
     * Проверить все введенные данные
     * @return `true`, если все данные валидны
     */
    private fun validateAndShowErrors(): Boolean {
        var allParamsOk = true

        val name = binding.passwordDetailsDomainNameInput.text.toString()
        if (!PasswordValidator.isNameValid(name)) {
            binding.passwordDetailsDomainNameInput.error =
                loadString(R.string.error_incorrect_password_name)
            allParamsOk = false
        }

        val link = binding.passwordDetailsDomainLinkInput.text.toString()
        if (!PasswordValidator.isLinkValid(link)) {
            binding.passwordDetailsDomainLinkInput.error =
                loadString(R.string.error_incorrect_password_link)
            allParamsOk = false
        }

        val pass = binding.passwordDetailsPasswordInput.text.toString()
        if (!PasswordValidator.isPasswordValid(pass)) {
            binding.passwordDetailsPasswordInput.error =
                loadString(R.string.error_incorrect_password_plain)
            allParamsOk = false
        }

        return allParamsOk
    }

    private fun loadString(@StringRes strId: Int): String = resources.getString(strId)

    private fun showError(@StringRes strId: Int) {
        Toast.makeText(context, strId, Toast.LENGTH_SHORT).show()
    }

    /**
     * Первоначальная установка скроллера с указанием длины
     */
    private fun setupHints() {
        if (passwordDetailsNavArgs.passwordId == 0L) {
            binding.passwordDetailsLastUpdate.visibility = View.GONE
            binding.passwordDetailsLastUpdateHint.visibility = View.GONE
        } else {
            binding.passwordDetailsLastUpdate.visibility = View.VISIBLE
            binding.passwordDetailsLastUpdateHint.visibility = View.VISIBLE
        }

        setupLengthIndicator(optionsBinding.generationOptionsLengthSlider.progress + PasswordValidator.MIN_LENGTH)
    }

    /**
     * Выводит данные о выбранной для генерации длине пароля
     */
    private fun setupLengthIndicator(len: Int) {
        optionsBinding.generationOptionsLabelLength.text =
            resources.getString(R.string.hint_length_marker, len)
    }
}