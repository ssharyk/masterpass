package free.ssharyk.masterpass.screens.list

import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import free.ssharyk.masterpass.screens.BaseViewModel
import free.ssharyk.masterpass.usecase.entity.Password
import free.ssharyk.masterpass.usecase.passwordcontent.PasswordsManager
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import javax.inject.Inject

@HiltViewModel
class PasswordsListViewModel @Inject constructor(
    private val passwordsManager: PasswordsManager
) : BaseViewModel() {

    /**
     * Получить все сохраненные пароли активного пользователя.
     * После извлечения сработает [callback]
     */
    fun retrieveUserPasswords(callback: (List<Password>) -> Unit) {
        viewModelScope.launch {
            withContext(Dispatchers.IO) {
                val passwords = passwordsManager.getAllPasswords()
                withContext(Dispatchers.Main) {
                    callback(passwords)
                }
            }
        }
    }

}