package free.ssharyk.masterpass.screens.auth

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import free.ssharyk.masterpass.screens.BaseViewModel
import free.ssharyk.masterpass.usecase.auth.AuthUseCase
import free.ssharyk.masterpass.usecase.auth.LoginResult
import free.ssharyk.masterpass.usecase.auth.SignUpResult
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import javax.inject.Inject

@HiltViewModel
open class AuthViewModel @Inject constructor(
    private val authUseCase: AuthUseCase
) : BaseViewModel() {

    private val _hasUsers = MutableLiveData<Boolean>()

    /**
     * Есть ли хотя бы одна учетная запись
     */
    val hasUsers: LiveData<Boolean> = _hasUsers

    private var isInProcess: Boolean = false

    init {
        // при старте, проверяем, есть ли хотя бы один пользователь
        viewModelScope.launch {
            val hasUsers = authUseCase.hasRecords()
            withContext(Dispatchers.Main) {
                _hasUsers.value = hasUsers
            }
        }
    }

    /**
     * Попытаться выполнить вход в аккаунт
     */
    fun login(name: String, password: String, callback: (LoginResult) -> Unit) {
        if (isInProcess) {
            return
        }
        isInProcess = true

        viewModelScope.launch {
            val res = authUseCase.login(name, password)
            withContext(Dispatchers.Main) {
                isInProcess = false
                callback(res)
            }
        }
    }

    /**
     * Попытаться создать новую учетную запись
     */
    fun register(name: String, password: String, callback: (SignUpResult) -> Unit) {
        if (isInProcess) {
            return
        }
        isInProcess = true

        viewModelScope.launch {
            val res = authUseCase.register(name, password)
            withContext(Dispatchers.Main) {
                isInProcess = false
                callback(res)
            }
        }
    }

}