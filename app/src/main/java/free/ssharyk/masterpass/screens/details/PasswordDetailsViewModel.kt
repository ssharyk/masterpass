package free.ssharyk.masterpass.screens.details

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import free.ssharyk.masterpass.screens.BaseViewModel
import free.ssharyk.masterpass.usecase.entity.Password
import free.ssharyk.masterpass.usecase.entity.PasswordGenerationOption
import free.ssharyk.masterpass.usecase.passwordcontent.PasswordsManager
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class PasswordDetailsViewModel @Inject constructor(
    private val passwordsManager: PasswordsManager
) : BaseViewModel() {

    private var passwordId: Long = 0L
    private var name: String = ""
    private var link: String = ""
    private var description: String = ""
    private var plainPassword: String = ""

    /**
     * Данные об активном пароле
     */
    val password: MutableLiveData<Password> = MutableLiveData()

    init {
        password.value = createPassword()
    }

    /**
     * Загрузить данные о пароле с идентификатором [passwordId] и передать их через [password]
     */
    fun load(passwordId: Long) {
        this.passwordId = passwordId

        if (passwordId == 0L) {
            // новый пароль
            password.postValue(createPassword())
        } else {
            viewModelScope.launch {
                val pass = passwordsManager.getPassword(passwordId)
                if (pass != null) {
                    password.postValue(pass)
                } else {
                    password.postValue(createPassword())
                }
            }
        }
    }

    /**
     * Запросить генерацию случайного пароля длины [length] с опциями генерации [options].
     * После завершения операции новый пароль будет передан в [password]
     */
    fun generateRandomPassword(length: Int, options: Set<PasswordGenerationOption>) {
        val newPassText = passwordsManager.createRandomPassword(
            length, options
        )
        this.plainPassword = newPassText

        val oldPassword = password.value ?: createPassword()
        val updatedPassword = oldPassword.copy(
            name = this.name,
            link = this.link,
            description = this.description,
            plainPassword = newPassText)
        password.postValue(updatedPassword)
    }

    /**
     * Сохранить название ресурса
     */
    fun updateName(newName: CharSequence?) {
        this.name = newName?.toString() ?: ""
    }

    /**
     * Сохранить указатель на ресурс
     */
    fun updateLink(newLink: CharSequence?) {
        this.link = newLink?.toString() ?: ""
    }

    /**
     * Сохранить описание ресурса
     */
    fun updateDescription(newDescription: CharSequence?) {
        this.description = newDescription?.toString() ?: ""
    }

    /**
     * Сохранить пароль, введенный пользователем
     */
    fun updatePassword(newPassword: CharSequence?) {
        this.plainPassword = newPassword?.toString() ?: ""
    }

    /**
     * Внести пароль в хранилище
     */
    fun save(
        name: String, link: String, description: String, passwordText: String,
        callback: (Boolean) -> Unit
    ) {
        viewModelScope.launch {
            val pass = password.value
            if (pass == null) {
                callback(false)
                return@launch
            }

            val passwordToSave = pass.copy(
                name = name,
                link = link,
                description = description,
                plainPassword = passwordText
            )
            val updateResult = passwordsManager.update(passwordToSave)
            callback(updateResult)
        }
    }

    private fun createPassword(): Password {
        return Password(passwordId, "", "", "", "", 0L)
    }

}