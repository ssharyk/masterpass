package free.ssharyk.masterpass.extensions

import android.content.Context
import android.util.AttributeSet
import com.google.android.material.floatingactionbutton.FloatingActionButton
import androidx.coordinatorlayout.widget.CoordinatorLayout
import android.view.View
import androidx.core.view.ViewCompat
import com.google.android.material.floatingactionbutton.FloatingActionButton.OnVisibilityChangedListener

class FloatingActionButtonScrollBehavior(context: Context?, attrs: AttributeSet?) :
    FloatingActionButton.Behavior(context, attrs) {

    override fun onStartNestedScroll(
        coordinatorLayout: CoordinatorLayout,
        child: FloatingActionButton,
        directTargetChild: View,
        target: View,
        axes: Int,
        type: Int
    ): Boolean {
        return if (axes == ViewCompat.SCROLL_AXIS_VERTICAL) {
            true
        } else {
            super.onStartNestedScroll(
                coordinatorLayout, child, directTargetChild,
                target, axes, type
            )
        }
    }

    override fun onNestedScroll(
        coordinatorLayout: CoordinatorLayout,
        child: FloatingActionButton,
        target: View,
        dxConsumed: Int,
        dyConsumed: Int,
        dxUnconsumed: Int,
        dyUnconsumed: Int,
        type: Int,
        consumed: IntArray
    ) {
        super.onNestedScroll(coordinatorLayout, child, target,
            dxConsumed, dyConsumed, dxUnconsumed, dyUnconsumed,
            type, consumed
        )

        /* If RecyclerView scroll action consumed vertical pixels bigger than 0, means scroll down. */
        if (dyConsumed > 0) {
            if (child.visibility == View.VISIBLE) {
                child.hide(object : OnVisibilityChangedListener() {
                    override fun onHidden(floatingActionButton: FloatingActionButton) {
                        super.onHidden(floatingActionButton)
                        floatingActionButton.visibility = View.INVISIBLE
                    }
                })
            }
        } else {
            if (dyConsumed < 0 && child.visibility != View.VISIBLE) {
                child.show()
            }
        }
    }
}