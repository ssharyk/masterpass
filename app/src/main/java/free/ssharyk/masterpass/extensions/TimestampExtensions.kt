package free.ssharyk.masterpass.extensions

import java.text.SimpleDateFormat
import java.util.*

private val sdf = SimpleDateFormat("dd-MM-yyyy  HH:mm")

fun Long.formattedDate(): String {
    return sdf.format(Date(this))
}