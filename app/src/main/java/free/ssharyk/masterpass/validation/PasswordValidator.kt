package free.ssharyk.masterpass.validation

object PasswordValidator {

    /**
     * Минимальная длина, которой должен обладать пароль
     */
    const val MIN_LENGTH = 12

    /**
     * Проверяет корректность мастер-пароля учетной записи
     */
    fun isMasterPasswordValid(password: String): Boolean {
        return isPasswordBaseValid(password)
    }

    /**
     * Проверяет корректность имени ресурса пароля
     */
    fun isNameValid(name: String): Boolean {
        return isNotEmpty(name)
    }

    /**
     * Проверяет корректность ссылки на ресурс пароля
     */
    fun isLinkValid(link: String): Boolean {
        return isNotEmpty(link)
    }

    /**
     * Проверяет корректность сохраненного пароля
     */
    fun isPasswordValid(password: String): Boolean {
        return isPasswordBaseValid(password)
    }

    /**
     * Общая проверка корректности пароля:
     *  - наличие пробелов
     *  - длина
     */
    private fun isPasswordBaseValid(password: String): Boolean {
        if (password.contains(' '))
            return false
        return password.length >= MIN_LENGTH
    }

    private fun isNotEmpty(str: String): Boolean {
        return str.trim().isNotEmpty()
    }

}