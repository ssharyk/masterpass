package free.ssharyk.masterpass.validation

object LoginValidator {

    /**
     * Проверяет корректность логина учетной записи
     */
    fun isLoginValid(login: String): Boolean {
        if (login.contains(' '))
            return false
        return login.length >= 8
    }

}