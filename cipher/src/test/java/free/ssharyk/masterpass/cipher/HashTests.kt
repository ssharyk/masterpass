package free.ssharyk.masterpass.cipher

import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.extension.ExtensionContext
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.Arguments
import org.junit.jupiter.params.provider.ArgumentsProvider
import org.junit.jupiter.params.provider.ArgumentsSource
import java.util.stream.Stream

class HashTests {

    @ParameterizedTest
    @ArgumentsSource(HashProvider::class)
    fun `when hash requested then calculated`(arg: HashArgument) {
        val actual = Digest.sha(arg.text)
        Assertions.assertEquals(arg.expectedDigest, actual)
    }


    data class HashArgument(val text: String, val expectedDigest: String)

    class HashProvider : ArgumentsProvider {
        override fun provideArguments(context: ExtensionContext?): Stream<out Arguments> =
            Stream.of(
                HashArgument(
                    "",
                    "da39a3ee5e6b4b0d3255bfef95601890afd80709"
                ),
                HashArgument(
                    "123456",
                    "7c4a8d09ca3762af61e59520943dc26494f8941b"
                ),
            ).map { Arguments.of(it) }
    }

}