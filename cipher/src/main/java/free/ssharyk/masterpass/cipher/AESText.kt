package free.ssharyk.masterpass.cipher

import android.util.Base64
import java.io.UnsupportedEncodingException
import javax.crypto.Cipher
import javax.crypto.SecretKey
import javax.crypto.spec.IvParameterSpec
import javax.crypto.spec.SecretKeySpec

/**
 * Реализация шифрования текста
 */
object AESText {

    // region Specification

    private const val ALGORITHM = "AES"
    private const val CIPHER_SPEC = "AES/CBC/PKCS5Padding"
    private const val OFFSET = 0
    private const val KEY_LENGTH = 16
    private const val IV_LENGTH = 16
    private const val IV_STRING = "ldFjG5UU8aBYWmuz"
    private var IV = ByteArray(IV_LENGTH)

    // endregion

    // region Main methods

    /**
     * Зашифровать [cleartext] ключом [password]
     */
    fun encrypt(cleartext: String?, password: String): String? {
        if (cleartext == null) return null
        val rawKey = getRawKey(password)
        val result = encrypt(rawKey, cleartext.toByteArray())
        return if (result != null) {
            bytesToString(result).trim { it <= ' ' }
        } else null
    }

    /**
     * Расшифровать [encrypted] ключом [password]
     */
    fun decrypt(encrypted: String, password: String): String {
        val enc = stringToBytes(encrypted)
        val result = decrypt(enc, password)
        return String(result)
    }

    /**
     * Зашифрование - основная реализация, на уровне байтов
     */
    private fun encrypt(raw: ByteArray, clear: ByteArray): ByteArray? {
        val skeySpec: SecretKey = SecretKeySpec(raw, OFFSET, KEY_LENGTH, ALGORITHM)
        return try {
            val cipher = Cipher.getInstance(CIPHER_SPEC)
            cipher.init(Cipher.ENCRYPT_MODE, skeySpec, IvParameterSpec(IV))
            cipher.doFinal(clear)
        } catch (e: Exception) {
            null
        }
    }

    /**
     * Расшифрование - основная реализация, на уровне байтов
     */
    private fun decrypt(encrypted: ByteArray, password: String): ByteArray {
        val skeySpec: SecretKey =
            SecretKeySpec(getKeyValue(password), OFFSET, KEY_LENGTH, ALGORITHM)
        val cipher = Cipher.getInstance(CIPHER_SPEC)
        cipher.init(Cipher.DECRYPT_MODE, skeySpec, IvParameterSpec(IV))
        return cipher.doFinal(encrypted)
    }

    // endregion

    // region Key

    /**
     * Преобразование ключа в набор байтов
     */
    private fun getRawKey(password: String): ByteArray {
        val key: SecretKey = SecretKeySpec(getKeyValue(password), OFFSET, KEY_LENGTH, ALGORITHM)
        return key.encoded
    }

    private fun getKeyValue(passwordOrigin: String): ByteArray {
        val len = passwordOrigin.length % 16
        val password = passwordOrigin.padEnd(passwordOrigin.length + 16 - len)
        val keyValue = ByteArray(password.length)
        for (i in 0 until password.length) {
            keyValue[i] = password[i].toByte()
        }
        return keyValue
    }

    // endregion

    // region Преобразования в/из Base64

    private fun bytesToString(arr: ByteArray): String {
        return Base64.encodeToString(arr, Base64.DEFAULT)
            .replace("\r", "")
            .replace("\n", "")
    }

    private fun stringToBytes(mes: String): ByteArray {
        return Base64.decode(mes, Base64.DEFAULT)
    }

    // endregion

    init {
        try {
            IV = IV_STRING.toByteArray(charset("UTF-8"))
        } catch (encExc: UnsupportedEncodingException) {
            encExc.printStackTrace()
            run {
                var i = 0
                while (i < IV_STRING.length) {
                    IV[i] = 0
                    i++
                }
            }
            var i = IV_STRING.length
            while (i < IV.size) {
                IV[i] = 49
                i++
            }
        }
    }
}