package free.ssharyk.masterpass.cipher

import kotlin.math.max
import kotlin.random.Random

object PasswordGenerator {

    private const val LETTERS = "abcdefghijklmnopqrstuvwxyz"
    private const val DIGITS = "0123456789"
    private const val SPECIALS = ".,!?#*_"

    private const val MIN_LENGTH = 12

    private const val SPECS_COUNT = 4
    private const val CASE_COUNT = 3

    fun create(
        wantedLength: Int,
        includeDigits: Boolean,
        includeSpecialChars: Boolean,
        includeTwoCases: Boolean
    ): String {
        val length = max(wantedLength, MIN_LENGTH)
        val sb = StringBuilder()

        append(sb, 2, if (includeDigits) DIGITS else LETTERS)
        append(sb, 2, if (includeSpecialChars) SPECIALS else LETTERS)
        append(sb, CASE_COUNT, if (includeTwoCases) LETTERS.toUpperCase() else LETTERS)
        append(sb, length - SPECS_COUNT - CASE_COUNT, LETTERS)
                
        return sb.toString().shuffled()
    }

    private fun append(sb: StringBuilder, count: Int, charactersSet: String) {
        for (i in 0 until count) {
            sb.append(randomChar(charactersSet))
        }
    }

    private fun randomChar(charactersSet: String): Char {
        val index = Random.nextInt(charactersSet.length)
        return charactersSet[index]
    }

    /**
     * Перемешивание символов в строке
     */
    private fun String.shuffled(): String {
        val chars = this.toCharArray()
        chars.shuffle()
        return String(chars)
    }

}
