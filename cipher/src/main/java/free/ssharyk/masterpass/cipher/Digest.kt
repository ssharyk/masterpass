package free.ssharyk.masterpass.cipher

import java.security.DigestException
import java.security.MessageDigest
import java.security.NoSuchAlgorithmException

internal object Digest {

    /**
     * SHA-1
     */
    fun sha(plainText: String): String? {
        var md: MessageDigest? = null
        try {
            md = MessageDigest.getInstance("SHA")
        } catch (e: NoSuchAlgorithmException) {
            e.printStackTrace()
            return null
        }

        try {
            md.update(plainText.toByteArray())
            val digest = md.digest()
            return byteArrayToHexString(digest)
        } catch (cnse: CloneNotSupportedException) {
            throw DigestException("couldn't make digest of partial content")
        }
    }

    /**
     * Преобразование набора байтов в шестнадцатеричное представление
     */
    private fun byteArrayToHexString(bytes: ByteArray): String {
        val hexString = StringBuilder()
        for (i in bytes.indices) {
            val hex = Integer.toHexString(bytes[i].toInt() and 0xFF)
            if (hex.length == 1) {
                hexString.append('0')
            }
            hexString.append(hex)
        }
        return hexString.toString()
    }

}