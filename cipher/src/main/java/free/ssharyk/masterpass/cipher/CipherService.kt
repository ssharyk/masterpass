package free.ssharyk.masterpass.cipher

interface CipherService {

    /**
     * Рассчитать хеш [plainText]
     */
    fun calculateHash(plainText: String): String?

    /**
     * Зашифровать [plainText] ключом [key]
     */
    fun encrypt(plainText: String, key: String): String?

    /**
     * Расшифровать [encryptedText] ключом [key]
     */
    fun decrypt(encryptedText: String, key: String): String?

    /**
     * Генерация случайного пароля
     *
     * @param length Длина пароля
     * @param includeDigits Включать ли цифры
     * @param includeSpecialChars Включать ли специальные символы
     * @param includeTwoCases Использовать ли символы в разном регистре
     *
     * @return Сгенерированный пароль
     */
    fun generate(
        length: Int,
        includeDigits: Boolean,
        includeSpecialChars: Boolean,
        includeTwoCases: Boolean
    ): String

}