package free.ssharyk.masterpass.cipher

class CipherModule : CipherService {

    override fun calculateHash(plainText: String): String? {
        return Digest.sha(plainText)
    }

    override fun encrypt(plainText: String, key: String): String? {
        return AESText.encrypt(plainText, key)
    }

    override fun decrypt(encryptedText: String, key: String): String? {
        return AESText.decrypt(encryptedText, key)
    }

    override fun generate(
        length: Int,
        includeDigits: Boolean,
        includeSpecialChars: Boolean,
        includeTwoCases: Boolean
    ): String {
        return PasswordGenerator.create(length, includeDigits, includeSpecialChars, includeTwoCases)
    }
}