package free.ssharyk.masterpass.cipher


import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton


@InstallIn(SingletonComponent::class)
@Module
object DiCipherModule {

    @Provides
    @Singleton
    fun provideCipherService(): CipherService {
        return CipherModule()
    }

}
