package free.ssharyk.masterpass.store.runtime

/**
 * Хранилище приватных данных в оперативной памяти
 */
interface RuntimeService {

    companion object {
        const val NO_PASSWORD = ""
        const val NO_USER_ID = -1L
    }


    /**
     * Мастер-пароль (в расшифрованном виде)
     */
    var masterPass: String

    /**
     * Идентификатор активного пользователя
     */
    var currentUserId: Long

}