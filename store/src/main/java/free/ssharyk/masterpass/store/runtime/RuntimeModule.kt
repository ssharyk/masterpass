package free.ssharyk.masterpass.store.runtime

import free.ssharyk.masterpass.store.runtime.RuntimeService.Companion.NO_PASSWORD
import free.ssharyk.masterpass.store.runtime.RuntimeService.Companion.NO_USER_ID

/**
 * Реализация ОЗУ-хранилища
 */
class RuntimeModule : RuntimeService {

    override var masterPass: String = NO_PASSWORD

    override var currentUserId: Long = NO_USER_ID

}