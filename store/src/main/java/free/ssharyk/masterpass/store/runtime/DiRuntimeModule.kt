package free.ssharyk.masterpass.store.runtime


import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton


@InstallIn(SingletonComponent::class)
@Module
object DiRuntimeModule {

    @Provides
    @Singleton
    fun provideRuntimeService(): RuntimeService {
        return RuntimeModule()
    }

}