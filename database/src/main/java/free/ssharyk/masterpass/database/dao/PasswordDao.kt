package free.ssharyk.masterpass.database.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Update
import free.ssharyk.masterpass.database.entity.Password

/**
 * Room DAO для паролей
 */
@Dao
interface PasswordDao {

    @Query("SELECT * FROM password where user_id = :userId")
    suspend fun findByUserId(userId: Long): List<Password>

    @Query("SELECT * FROM password where id = :passwordId")
    suspend fun findById(passwordId: Long): Password?

    @Insert
    suspend fun insert(password: Password): Long

    @Update
    suspend fun update(password: Password): Int

}