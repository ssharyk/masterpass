package free.ssharyk.masterpass.database.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

/**
 * Таблица учетных записей в БД
 */
@Entity(tableName = "users")
data class UserRecord(
    @PrimaryKey(autoGenerate = true) val id: Long,
    @ColumnInfo(name = "name") val name: String,
    @ColumnInfo(name = "password_hash") val passwordHash: String
)