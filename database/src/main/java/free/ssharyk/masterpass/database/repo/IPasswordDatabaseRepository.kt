package free.ssharyk.masterpass.database.repo

import free.ssharyk.masterpass.database.entity.Password

/**
 * Управление паролями в хранилище
 */
interface IPasswordDatabaseRepository {

    /**
     * Получить пароль с иденификатором [passwordId]
     */
    suspend fun findPasswordById(passwordId: Long): Password?

    /**
     * Получить пароли, принадлежащие учетной записи [userId]
     */
    suspend fun findPasswordsByUserId(userId: Long): List<Password>

    /**
     * Добавить новую или изменить существующую (по идентификатору) запись о пароле
     */
    suspend fun insertOrReplacePassword(
        id: Long,
        userId: Long,
        title: String,
        link: String,
        encryptedPassword: String,
        description: String?
    ): Password?

}