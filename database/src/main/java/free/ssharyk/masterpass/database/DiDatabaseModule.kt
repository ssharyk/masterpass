package free.ssharyk.masterpass.database

import android.content.Context
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import free.ssharyk.masterpass.database.repo.DatabaseModule
import free.ssharyk.masterpass.database.repo.IPasswordDatabaseRepository
import free.ssharyk.masterpass.database.repo.IUserDatabaseRepository
import javax.inject.Singleton


@InstallIn(SingletonComponent::class)
@Module
object DiDatabaseModule {

    private lateinit var db: DatabaseModule

    @Provides
    @Singleton
    fun provideUserDatabaseRepository(@ApplicationContext context: Context): IUserDatabaseRepository {
        return getOrCreateDatabaseModule(context)
    }

    @Provides
    @Singleton
    fun providePasswordDatabaseRepository(@ApplicationContext context: Context): IPasswordDatabaseRepository {
        return getOrCreateDatabaseModule(context)
    }

    private fun getOrCreateDatabaseModule(context: Context): DatabaseModule {
        if (!this::db.isInitialized) {
            db = DatabaseModule(context)
        }
        return db
    }

}
