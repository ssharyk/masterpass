package free.ssharyk.masterpass.database

import android.content.Context
import androidx.room.Room

/**
 * Вспомогательный класс для порождения единственного экземпляра БД
 */
object DatabaseBuilder {

    private const val DB_NAME = "masterpassdb"

    private var INSTANCE: AppDatabase? = null

    internal fun getInstance(context: Context): AppDatabase {
        if (INSTANCE == null) {
            synchronized(AppDatabase::class) {
                INSTANCE = buildRoomDB(context)
            }
        }
        return INSTANCE!!
    }

    private fun buildRoomDB(context: Context) =
        Room.databaseBuilder(
            context.applicationContext,
            AppDatabase::class.java,
            DB_NAME
        ).build()

}