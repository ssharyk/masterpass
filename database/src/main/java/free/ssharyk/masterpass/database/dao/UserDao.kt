package free.ssharyk.masterpass.database.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import free.ssharyk.masterpass.database.entity.UserRecord

/**
 * Room DAO для учетных записей
 */
@Dao
interface UserDao {

    @Query("SELECT * FROM users WHERE name = :name")
    suspend fun findByLogin(name: String): UserRecord?

    @Insert
    suspend fun insert(user: UserRecord): Long

    @Query("SELECT COUNT(id) FROM users")
    suspend fun count(): Int

}