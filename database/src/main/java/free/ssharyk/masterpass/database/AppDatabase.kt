package free.ssharyk.masterpass.database

import androidx.room.Database
import androidx.room.RoomDatabase
import free.ssharyk.masterpass.database.dao.PasswordDao
import free.ssharyk.masterpass.database.dao.UserDao
import free.ssharyk.masterpass.database.entity.Password
import free.ssharyk.masterpass.database.entity.UserRecord


/**
 * База данных Room
 */
@Database(entities = [UserRecord::class, Password::class], version = 1)
internal abstract class AppDatabase : RoomDatabase() {

    abstract fun userDao(): UserDao

    abstract fun passwordDao(): PasswordDao

}