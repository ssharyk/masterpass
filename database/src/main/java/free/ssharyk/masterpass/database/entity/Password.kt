package free.ssharyk.masterpass.database.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.PrimaryKey

/**
 * Таблица Пароль в БД
 */
@Entity(
    foreignKeys = [ForeignKey(
        entity = UserRecord::class, parentColumns = ["id"], childColumns = ["user_id"],
        onDelete = ForeignKey.CASCADE
    )]
)
data class Password(
    @PrimaryKey(autoGenerate = true) val id: Long,
    @ColumnInfo(name = "user_id") val userId: Long,
    @ColumnInfo(name = "password") val encryptedPassword: String,
    @ColumnInfo(name = "title") val name: String,
    @ColumnInfo(name = "link") val link: String,
    @ColumnInfo(name = "description") val description: String?,
    @ColumnInfo(name = "timestamp") val timestamp: Long,
)