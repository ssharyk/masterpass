package free.ssharyk.masterpass.database.repo

import android.content.Context
import free.ssharyk.masterpass.database.DatabaseBuilder
import free.ssharyk.masterpass.database.entity.Password
import free.ssharyk.masterpass.database.entity.UserRecord

/**
 * Реализация работы с хранилищем, которая использует Room DAO
 */
class DatabaseModule(context: Context) : IUserDatabaseRepository, IPasswordDatabaseRepository {

    private val db = DatabaseBuilder.getInstance(context)

    // region User

    override suspend fun usersCount(): Int {
        return db.userDao().count()
    }

    override suspend fun findUserByLogin(name: String): UserRecord? {
        return db.userDao().findByLogin(name)
    }

    override suspend fun insertUser(name: String, masterPasswordHash: String): UserRecord? {
        val userRecord = UserRecord(0L, name, masterPasswordHash)
        val id = db.userDao().insert(userRecord)
        return userRecord.copy(id = id)
    }

    // endregion

    // region Passwords

    override suspend fun findPasswordsByUserId(userId: Long): List<Password> {
        return db.passwordDao().findByUserId(userId)
    }

    override suspend fun findPasswordById(passwordId: Long): Password? {
        return db.passwordDao().findById(passwordId)
    }

    override suspend fun insertOrReplacePassword(
        id: Long,
        userId: Long,
        title: String,
        link: String,
        encryptedPassword: String,
        description: String?
    ): Password? {
        val timestamp = System.currentTimeMillis()
        val passwordEntity = Password(id, userId, encryptedPassword, title, link, description, timestamp)
        return if (id == 0L) {
            val createdId = db.passwordDao().insert(passwordEntity)
            if (createdId != 0L) {
                passwordEntity.copy(id = createdId)
            } else {
                null
            }
        } else {
            val updatedCount = db.passwordDao().update(passwordEntity)
            if (updatedCount == 0) {
                null
            } else {
                passwordEntity
            }
        }
    }

    // endregion

}