package free.ssharyk.masterpass.database.repo

import free.ssharyk.masterpass.database.entity.UserRecord

/**
 * Управление учетными записями в хранилизе
 */
interface IUserDatabaseRepository {

    /**
     * Количество записей
     */
    suspend fun usersCount(): Int

    /**
     * Поиск учетной записи по логину [name]
     */
    suspend fun findUserByLogin(name: String): UserRecord?

    /**
     * Добавление новой учетной записи с логином [name] и хешем мастер-пароля [masterPasswordHash]
     */
    suspend fun insertUser(name: String, masterPasswordHash: String): UserRecord?

}